from setuptools import find_packages, setup

setup(
    name='robot_arm_wood',
    version='1.0.0',
    url='http://www.dabbmedia.com/software/',
    license='BSD',
    maintainer='Brent Self',
    maintainer_email='brentself@gmail.com',
    description='Control for a robot arm that uses stepper motors.',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
)