# usage
# pip3 install -e .
# flask init-db
# export FLASK_APP=robot_arm_wood
# export FLASK_ENV=development
# flask run --host=0.0.0.0
from flask import (
    Blueprint, jsonify, redirect, render_template, request, url_for
)

import time

import RPi.GPIO as GPIO

from robot_arm_wood.db import get_db

bp = Blueprint('arm', __name__)

@bp.route("/")
def index():
    db = get_db()
    robot_states = db.execute(
        'SELECT id, name, description, type, value, minValue, maxValue, updated, pin1, pin2, pin3, pin4'
        ' FROM robot_state '
        ' ORDER BY name ASC'
    ).fetchall()
    return render_template('arm/index.html', robot_states=robot_states)

@bp.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']
        type = request.form['type']
        value = request.form['value']
        minValue = request.form['min-value']
        maxValue = request.form['max-value']
        error = None

        if not name:
            error = 'Name is required.'
        if not description:
            error = 'Description is required.'
        if not type:
            error = 'Type is required.'
        if not value:
            error = 'Default value is required.'
        if not minValue:
            error = 'Min value is required.'
        if not maxValue:
            error = 'Max value is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO robot_state (name, description, type, value, minValue, maxValue)'
                ' VALUES (?, ?, ?, ?, ?)',
                (name, description, type, value, minValue, maxValue)
            )
            db.commit()
            return redirect(url_for('arm.index'))

    return render_template('arm/create.html')

def get_robot_state(id):
    robot_state = get_db().execute(
        'SELECT id, name, description, type, value, minValue, maxValue, updated, pin1, pin2, pin3, pin4'
        ' FROM robot_state'
        ' WHERE id = ?'
        ' ORDER BY name ASC',
        (id,)
    ).fetchone()
    if robot_state is None:
        abort(404, "robot_state id {0} doesn't exist.".format(id))

    return robot_state

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
def update(id):
    robot_state = get_robot_state(id)

    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']
        type = request.form['type']
        value = request.form['value']
        minValue = request.form['min-value']
        maxValue = request.form['max-value']
        pin1 = request.form['pin1']
        pin2 = request.form['pin2']
        pin3 = request.form['pin3']
        pin4 = request.form['pin4']
        error = None

        if not name:
            error = 'Name is required.'
        if not type:
            error = 'Type is required.'
        if not value:
            error = 'Value is required.'
        if not minValue:
            error = 'Minimum value is required.'
        if not maxValue:
            error = 'Maximum value is required.'
        if not pin1:
            pin1 = 0
        if not pin2:
            pin2 = 0
        if not pin3:
            pin3 = 0
        if not pin4:
            pin4 = 0

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE robot_state SET name = ?, type = ?, value = ?, minValue = ?, maxValue = ?, pin1 = ?, pin2 = ?, pin3 = ?, pin4 = ?'
                ' WHERE id = ?',
                (name, type, value, minValue, maxValue, pin1, pin2, pin3, pin4, id)
            )
            db.commit()
            return redirect(url_for('arm.index'))

    return render_template('arm/update.html', robot_state=robot_state)

@bp.route('/<int:id>/update-value', methods=('GET', 'POST'))
def updateValue(id):
    if request.method == 'POST':
        value = request.form['value']
        direction = request.form['direction']
        pin1 = int(request.form['pin1'])
        pin2 = int(request.form['pin2'])
        pin3 = int(request.form['pin3'])
        pin4 = int(request.form['pin4'])
        error = None

        if not value:
            error = 'Value is required.'
            return jsonify(error)

        if error is not None:
            return jsonify(error)
        else:
            GPIO.setmode(GPIO.BOARD)
            pin_list = [pin1, pin2, pin3, pin4]
            GPIO.setup(pin_list, GPIO.OUT)
            # pin pulses, per motor docs
            seqs = [[1,0,0,0], 
             [1,1,0,0],
             [0,1,0,0],
             [0,1,1,0],
             [0,0,1,0],
             [0,0,1,1],
             [0,0,0,1],
             [1,0,0,1]]

            seqsReverse = [[0,1,0,0],
             [0,1,1,0],
             [1,0,0,0], 
             [1,1,0,0],
             [0,0,0,1],
             [1,0,0,1],
             [0,0,1,0],
             [0,0,1,1]]
            #send signal to move motor
            ratio = int(4096 / 360)
            for i in range(ratio):
                if direction == 'down':
                    direction_seq = seqsReverse
                else:
                    direction_seq = seqs
                for seq in direction_seq:
                    GPIO.output(pin_list, seq)
                    time.sleep(0.001)
                GPIO.output([pin1, pin2, pin3, pin4], GPIO.LOW)
            #update the position in db
            db = get_db()
            db.execute(
                'UPDATE robot_state SET value = ?'
                ' WHERE id = ?',
                (value, id)
            )
            db.commit()
            return jsonify('success')
    else:
        return jsonify('Error, not a POST request.')

@bp.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM robot_state WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('arm.index'))