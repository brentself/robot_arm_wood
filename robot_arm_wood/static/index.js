$(function() {
  $('.robot_state').each(function(index) {
    $(this).find('#up').bind('click', function() {
      var parentElement = $(this).parent();
      var currentValue = parentElement.find('#value').val();
      currentValue = Number((currentValue == '') ? currentValue = 0 : currentValue);
      var maxValue = parentElement.find('#max_value').val();
      maxValue = Number((maxValue == '') ? maxValue = 0 : maxValue);
      if (currentValue < maxValue) {
        var newValue = currentValue + 1;
        parentElement.find('#value').val(newValue);
        updateState(parentElement, 'up');
      }
      return false;
    });

    $(this).find('#down').bind('click', function() {
      var parentElement = $(this).parent();
      var currentValue = parentElement.find('#value').val();
      currentValue = Number((currentValue == '') ? currentValue = 0 : currentValue);
      var minValue = parentElement.find('#min_value').val();
      minValue = Number((minValue == '') ? minValue = 0 : minValue);
      if (currentValue > minValue) {
        var newValue = currentValue - 1;
        parentElement.find('#value').val(newValue);
        updateState(parentElement, 'down');
      }
      return false;
    });
  });

  var updateState = function(data, direction) {
    $.post('/' + data.find('#id').val() + '/update-value', {
      id: data.find('#id').val(),
      value: data.find('#value').val(),
      direction: direction,
      pin1: Number(data.find('#pin1').html()),
      pin2: Number(data.find('#pin2').html()),
      pin3: Number(data.find('#pin3').html()),
      pin4: Number(data.find('#pin4').html())
    }, function(data) {
      // $("#result").text(data.result);
    });
  };
});