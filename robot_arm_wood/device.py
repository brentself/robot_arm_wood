import RPi.GPIO as GPIO

from robot_arm_wood.db import get_db

def init_device(app):
    app.teardown_appcontext(init_gpio)

# setup the pins/connections for each motor/joint
def init_gpio(e=None):
    # GPIO.BCM is the other mode
    GPIO.setmode(GPIO.BOARD)

    # get the 4 pin numbers for each motor/joint
    # from the sqlite database
    db = get_db()
    robot_states = db.execute(
        'SELECT id, pin1, pin2, pin3, pin4'
        ' FROM robot_state '
        ' ORDER BY name ASC'
    ).fetchall()
    
    for robot_state in robot_states:
        # set all 4 pins as outputs
        pin1 = int(robot_state['pin1'])
        pin2 = int(robot_state['pin2'])
        pin3 = int(robot_state['pin3'])
        pin4 = int(robot_state['pin4'])
        pin_list = [pin1, pin2, pin3, pin4]
        GPIO.setup(pin_list, GPIO.OUT)

# reset the GPIO pins used when app closes
def close_gpio(e=None):
    GPIO.cleanup()