DROP TABLE IF EXISTS robot_state;

CREATE TABLE robot_state (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name VARCHAR(256) NOT NULL,
  description TEXT NULL,
  type VARCHAR(256) NOT NULL,
  value TEXT NOT NULL,
  minValue TEXT NOT NULL,
  maxValue TEXT NOT NULL,
  pin1 INTEGER NULL,
  pin2 INTEGER NULL,
  pin3 INTEGER NULL,
  pin4 INTEGER NULL
);

INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 0', 'Spins the base of the arm. Attached to the base.', 'int', 0, 0, 270, 3, 5, 7, 11);
INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 1', 'Adjusts the angle of the arm off of the base, moves the longest segment. Attached to short segment on top of base.', 'int', 0, 0, 200, 13, 15, 19, 21);
INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 2', 'Adjusts the angle of the 2nd longest segment of the arm. Attached to end of longest segment.', 'int', 0, 0, 270, 23, 29, 31, 33);
INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 3', 'Adjusts the forward/back angle of the endpoint. Attached to end of the 2nd longest segment.', 'int', 0, 0, 270, 35, 37, 40, 38);
INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 4', 'Adjusts the side to side angle of the endpoint. Attached to short segment.', 'int', 0, 0, 320, 8, 10, 12, 16);
INSERT INTO robot_state (name, description, type, value, minValue, maxValue, pin1, pin2, pin3, pin4) VALUES ('Joint 5', 'Opens and closes the endpoint.', 'int', 0, 0, 240, 18, 22, 24, 26);
